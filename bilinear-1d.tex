\documentclass{amsart}
\input{macros.tex}
\usepackage{forest}
\newcommand{\A}[4]{
  \ensuremath{\boxed{\begin{matrix}#1 &#2\\#3&#4 \end{matrix}}}
}% {scale1}{scale2}{exponent1}{exponent2}
\begin{document}

In this note I record the numerology of bilinear proofs of decoupling for moment curves.

\section{Notation}
Let $p_{l}=l(l+1)$.
Let $\theta,\theta'$ be transverse $1/K$-arcs.
I write
\[
A_{b,b'}^{p,p'} := \delta^{b(1/2-1/p)+b'(1/2-1/p')} \Bigl( \sum_{\tau \in \Part[\theta]{\delta^{b}}} \sum_{\tau' \in \Part[\theta']{\delta^{b'}}} \int_{\R^{k}} \abs{f_{\tau}}^{p} \abs{f_{\tau'}}^{p'} \Bigr)^{1/p_{k}},
\]
where $p'$ is not the H\"older conjugate, but rather some exponent with $p+p'=p_{k}$.

Then, by lower degree decoupling, for $1 \leq l < k$ we have
\[
A_{a,b'}^{p_{l},p_{k}-p_{l}}
\lessapprox
A_{(k-l+1)b'/l,b'}^{p_{l},p_{k}-p_{l}}.
\]
Also we can change exponents $p,p'$ by H\"older's inequality.
Let $a_{l}(b)$ be the infimum of all $a>0$ such that we have
\[
A_{(k-l+1)b/l,b}^{p_{l},p_{k}-p_{l}} \lesssim \delta^{-a}
(\ell^{p}_{\tau\in\Part[\theta]{\delta}} \norm{f_{\tau}})^{p_{l}/p_{k}}
(\ell^{p}_{\tau\in\Part[\theta']{\delta}} \norm{f_{\tau}})^{1-p_{l}/p_{k}}.
\]
I abbreviate $A_{b,b'}^{p,p'}$ to
\[
\A{b}{b'}{p}{p'}
\]
in tree diagrams below.
Branching in the graphs means that we apply H\"older's inequality, while non-branching means that we apply lower degree decoupling.
After obtaining inequalities for $a_{*}(b)$ we can conclude as in the ACK paper.

The condition on H\"older exponents that permits branching like this:

\begin{forest}
[ \A{b}{b'}{p}{p'}
[ \A{b}{b'}{p_0}{p'_0},edge label={node[midway,left] {$1-\theta$}}]
[ \A{b}{b'}{p_1}{p'_1},edge label={node[midway,right] {$\theta$}}]];
\end{forest}

is
\[
p=(1-\theta)p_{0}+\theta p_{1},
\quad
p'=(1-\theta)p'_{0}+\theta p'_{1}.
\]

\section{Degree 2}
\begin{forest}
[ \A{2b}{b}{2}{4}
[ \A{2b}{b}{0}{6},edge label={node[midway,left] {$1/2$}}]
[ \A{2b}{b}{4}{2},edge label={node[midway,right] {$1/2$}}
[ \A{2b}{4b}{4}{2} ]]];
\end{forest}
\begin{align*}
a_{1}(b) &\leq \frac{1}{2}\eta b + \frac{1}{2} a_{1}(2b)
\end{align*}
Iteration matrix
\[
\begin{pmatrix}
1
\end{pmatrix}
\]
has PF eigenvalue $1$.

\section{Dimension 1, general degree}
$p_{l}=l(l+1)$ for $l=0,1,\dotsc$.

\begin{forest}
[ \A{(k-l+1)b/l}{b}{p_l}{p_k-p_l}
[ \A{(k-l+1)b/l}{b}{p_{l-1}}{p_k-p_{l-1}},edge label={node[midway,left] {$1-\theta_{l}$}}
[ \A{(k-l+2)b/(l-1)}{b}{p_{l-1}}{p_k-p_{l-1}}]]
[ \A{(k-l+1)b/l}{b}{p_k-p_{k-l}}{p_{k-l}},edge label={node[midway,right] {$\theta_{l}$}}
[ \A{(k-l+1)b/l}{\frac{k-l+1}{l} \frac{l+1}{k-l}b}{p_k-p_{k-l}}{p_{k-l}} ]]];
\end{forest}

In both branches we choose the smallest exponents for which we can apply some lower degree sharp decoupling.

For $1\leq l<k$, $\theta_{l}$ is determined by
\[
(1-\theta_{l}) p_{l-1} + \theta_{l} (p_{k}-p_{k-l}) = p_{l},
\]
hence with $p_{l}=l(l+1)$ we obtain
\[
\theta_{l} = \frac{p_{l}-p_{l-1}}{p_{k}-p_{k-l}-p_{l-1}} = \frac{1}{k-l+1}.
\]
This gives us
\begin{align*}
a_{l}(b) &\leq (1-\theta_{l}) a_{l-1}(b) + \theta_{l} a_{k-l}(\frac{k-l+1}{l}b), & 1\leq l<k.
\end{align*}
The entries of the associated matrix are given by
\begin{align*}
m_{ll'}
&=
\one_{l'=k-l} \theta_{l}\frac{k-l+1}{l} + \one_{l'=l-1} (1-\theta_{l})
\\ &=
\one_{l'=k-l} \frac{1}{l} + \one_{l'=l-1} \frac{k-l}{k-l+1},
\end{align*}
where $1 \leq l,l' < k$.
For every $l'$ we have
\[
\sum_{l} m_{ll'}
=
\frac{1}{k-l'} + \one_{l'\neq k-1} \frac{k-(l'+1)}{k-(l'+1)+1}
=
\frac{1}{k-l'} + \frac{k-l'-1}{k-l'}
=
1.
\]
Hence the matrix $(m_{ll'})$ has left PF eigenvector $(1,\dotsc,1)$, and its PF eigenvalue is $1$.

\section{General degree, higher dimension}
Disregard lower dimensional contributions.

For transversality to work as before we should assume
\begin{equation}
\label{eq:1}
n_{1}+\dotsb+n_{l} \leq n_{k}+\dotsb+n_{k-l+1},
\end{equation}
where $n_{l}$ is the number of degree $l$ polynomials in the reduced TDI system.
Let also
\[
\calK_{m} := \sum_{l=1}^{m} l n_{l}
\]
be the homogeneous dimension.

Suppose $p_{l} = p \frac{\calK_{l}}{\calK_{k}}$, this is the choice of lower degree exponents that worked in the ACK paper for large $p$.
Then the same H\"older and lower degree decoupling as above works with
\[
\theta_{l} = \frac{\calK_{l}-\calK_{l-1}}{\calK_{k}-\calK_{k-l}-\calK_{l-1}}.
\]
The denominator is strictly positive by \eqref{eq:1}.
The problem is whether the $1 \leq l,l' < k$ matrix
\[
m_{ll'}
=
\one_{l'=k-l} \theta_{l}\frac{k-l+1}{l} + \one_{l'=l-1} (1-\theta_{l})
\]
has PF eigenvalue $\geq 1$.

We have
\[
\theta_{1}
=
\frac{\calK_{1}-\calK_{0}}{\calK_{k}-\calK_{k-1}-\calK_{0}}.
=
\frac{n_{1}}{k n_{k}}.
\]

\subsection{Degree 3}
In this case the matrix becomes
\[
\begin{pmatrix}
0 & 3\theta_{1}\\
1 & 0
\end{pmatrix}
=
\begin{pmatrix}
0 & n_{1}/n_{3}\\
1 & 0
\end{pmatrix}.
\]
When \eqref{eq:1} holds, the PF eigenvalue is $1$ only if $n_{1}=n_{3}$.

This is consistent with the our proof of sharp decoupling for the surface $(\xi,\eta,\xi^{2},\xi\eta,\eta^{2},\xi^{3},\eta^{3})$ using the bilinear approach.

\subsection{Higher degree}
Assume
\[
n_{l} = n_{k-l+1}
\]
for all $1 \leq l < k$.
Then one can verify
\[
\theta_{k-l'} \frac{l'+1}{k-l'} = \theta_{l'+1}
\]
for all $1\leq l' < k$, and $\theta_{1}k = n_{1}/n_{k}=1$.
It follows that $(1,\dotsc,1)$ is a left PF eigenvector with eigenvalue $1$.

\section{Parsell--Vinogradov, dimension 2, degree 4}
Her critical exponents are $p_{l}=\calK_{l}$, in particular, $p_{1}=2$, $p_{2}=8$, $p_{3}=20$, $p_{4}=40$.
Let $k=4$.
A particularity of this case is possible transversality between $V_{2}(\xi)$ and $V_{3}(\xi')$.
\begin{forest}
[ \A{4b}{b}{2}{38}
[ \A{4b}{b}{0}{40},edge label={node[midway,left] {$9/10$}}]
[ \A{4b}{b}{20}{20},edge label={node[midway,right] {$1/10$}}
[ \A{4b}{4b}{20}{20} ]]];
\end{forest}
\begin{forest}
[ \A{2b}{b}{8}{32}
[ \A{2b}{b}{2}{38},edge label={node[midway,left] {$2/3$}}
[ \A{4b}{b}{2}{38}]]
[ \A{2b}{b}{20}{20},edge label={node[midway,right] {$1/3$}}
[ \A{2b}{2b}{20}{20} ]]];
\end{forest}
\begin{forest}
[ \A{b}{b}{20}{20}
[ \A{b}{b}{8}{32},edge label={node[midway,left] {$1/2$}}
[ \A{2b}{b}{8}{32}]]
[ \A{b}{b}{32}{8},edge label={node[midway,right] {$1/2$}}
[ \A{b}{2b}{32}{8} ]]];
\end{forest}
\begin{align*}
a_{1}(b) &\leq \frac{9}{10}\eta b + \frac{1}{10} a_{3}(4b)\\
a_{2}(b) &\leq \frac{2}{3}a_{1}(b) + \frac{1}{3} a_{3}(2b)\\
a_{3}(b) &\leq \frac{1}{2}a_{2}(b) + \frac{1}{2} a_{2}(b)
\end{align*}
Iteration matrix
\[
\begin{pmatrix}
0 & 0 & 4/10\\
2/3 & 0 & 2/3\\
0 & 1 & 0
\end{pmatrix}
\]
has PF eigenvalue $<1$, so the iteration does not work.


\section{Parsell--Vinogradov, dimension 2, degree 2}
Her critical exponents are $p_{l}=\calK_{l}$, $p_{1}=2$, $p_{2}=8$.
Let $k=2$.

\begin{forest}
[ \A{2b}{b}{2}{6}
[ \A{2b}{b}{0}{8},edge label={node[midway,left] {$2/3$}}]
[ \A{2b}{b}{6}{2},edge label={node[midway,right] {$1/3$}}
[ \A{2b}{4b}{6}{2} ]]];
\end{forest}

PF eigenvalue is $2\cdot 1/3<1$, so the iteration does not work.

\subsection{An iteration that might work}
We know that for the system $(\xi,\eta,\xi\eta)$ the exponent $4$ is critical.
Maybe we can use this on $3$-dimensional subspaces of $\R^{5}$.
Then the following iteration would work:

\begin{forest}
[ \A{2b}{b}{2}{6}
[ \A{2b}{b}{0}{8},edge label={node[midway,left] {$1/2$}}]
[ \A{2b}{b}{4}{4},edge label={node[midway,right] {$1/2$}}
[ \A{2b}{2b}{4}{4}
[ \A{2b}{2b}{6}{2},edge label={node[midway,left] {$1/2$}}
[ \A{2b}{4b}{6}{2} ]]
[ \A{2b}{2b}{2}{6},edge label={node[midway,right] {$1/2$}}
[ \A{4b}{2b}{6}{2} ]]]]];
\end{forest}

\section{14-example}
$p_{1}=2$, $p_{2}=8$, $p_{3}=14$.

\begin{forest}
[ \A{3b}{b}{2}{12}
[ \A{3b}{b}{0}{14},edge label={node[midway,left] {$2/3$}}]
[ \A{3b}{b}{6}{8},edge label={node[midway,right] {$1/3$}}
[ \A{3b}{3b}{6}{8} ]]];
\end{forest}
\begin{forest}
[ \A{b}{b}{8}{6}
[ \A{b}{b}{2}{12},edge label={node[midway,left] {$2/5$}}
[ \A{2b}{b}{2}{12}]]
[ \A{b}{b}{12}{2},edge label={node[midway,right] {$3/5$}}
[ \A{b}{2b}{12}{2} ]]];
\end{forest}

This iteration clearly has eigenvalue $1$.

\section{17-example}
$p_{1}=2$, $p_{2}=8$, $p_{3}=17$.
Assume that we know sharp decoupling for the $11$-example; we will also use this exponent.

\begin{forest}
[ \A{3b}{b}{2}{15}
[ \A{3b}{b}{0}{17},edge label={node[midway,left] {$2/3$}}]
[ \A{3b}{b}{6}{11},edge label={node[midway,right] {$1/3$}}
[ \A{3b}{2b}{6}{11} ]]];
\end{forest}
\begin{forest}
[ \A{3b/2}{b}{4}{13}
[ \A{3b/2}{b}{2}{15},edge label={node[midway,left] {$5/7$}}
[ \A{3b}{b}{2}{15}]]
[ \A{3b/2}{b}{9}{8},edge label={node[midway,right] {$2/7$}}
[ \A{3b/2}{3b/2}{9}{8} ]]];
\end{forest}
\begin{forest}
[ \A{b}{b}{8}{9}
[ \A{b}{b}{4}{13},edge label={node[midway,left] {$?$}}
[ \A{3b/2}{b}{4}{13}]]
[ \A{b}{b}{13}{4},edge label={node[midway,right] {$?$}}
[ \A{b}{3b/2}{13}{4} ]]];
\end{forest}
\begin{forest}
[ \A{2b/3}{b}{11}{6}
[ \A{2b/3}{b}{8}{9},edge label={node[midway,left] {$4/7$}}
[ \A{b}{b}{8}{9}]]
[ \A{2b/3}{b}{15}{2},edge label={node[midway,right] {$3/7$}}
[ \A{2b/3}{2b}{15}{2} ]]];
\end{forest}

The matrix of this iteration is
\[
\begin{pmatrix}
0 & 0 & 0 & 1\\
\frac57 & 0 & \frac27 \cdot \frac32 & 0\\
0 & 1 & 0 & 0\\
\frac37 \cdot \frac23 & 0 & \frac47 & 0
\end{pmatrix}
\]
This matrix is row stochastic, so its PF eigenvalue is $1$.

We already know how to prove the sharp $L^{17}$ decoupling for this system by the multilinear approach.

\section{(6,12)-example}
$p_{1}=2$, $p_{2}=4$, $p_{3}=12$.
There is necessarily a loss at $L^{12}$.

\begin{forest}
[ \A{3b}{b}{2}{10}
[ \A{3b}{b}{0}{12},edge label={node[midway,left] {$2/3$}}]
[ \A{3b}{b}{6}{6},edge label={node[midway,right] {$1/3$}}
[ \A{3b}{3b/2}{6}{6} ]]];
\end{forest}
\begin{forest}
[ \A{b}{b}{4}{8}
[ \A{b}{b}{2}{10},edge label={node[midway,left] {$?$}}
[ \A{3b}{b}{2}{10}]]
[ \A{b}{b}{10}{2},edge label={node[midway,right] {$?$}}
[ \A{b}{3b}{10}{2} ]]];
\end{forest}
\begin{forest}
[ \A{b/2}{b}{6}{6}
[ \A{b/2}{b}{4}{8},edge label={node[midway,left] {$2/3$}}
[ \A{b}{b}{4}{8}]]
[ \A{b/2}{b}{10}{2},edge label={node[midway,right] {$1/3$}}
[ \A{b/2}{3b/2}{10}{2} ]]];
\end{forest}

This iteration has eigenvalue $<1$.
But we know how to prove the optimal $L^{12}$ result by multilinear approach.
Is there a bilinear proof?

\section{11-example}
$p_{1}=2$, $p_{2}=6$.
I don't know how to prove sharp $L^{11}$ decoupling, but the following scheme seems to give sharp $L^{10}$ decoupling:

\begin{forest}
[ \A{2b}{b}{2}{8}
[ \A{2b}{b}{0}{10},edge label={node[midway,left] {$1/2$}}]
[ \A{2b}{b}{4}{6},edge label={node[midway,right] {$1/2$}}
[ \A{2b}{2b}{4}{6} ]]];
\end{forest}
\begin{forest}
[ \A{b}{b}{6}{4}
[ \A{b}{b}{2}{8},edge label={node[midway,left] {$1/3$}}
[ \A{2b}{b}{2}{8}]]
[ \A{b}{b}{8}{2},edge label={node[midway,right] {$2/3$}}
[ \A{b}{2b}{8}{2} ]]];
\end{forest}


\end{document}
